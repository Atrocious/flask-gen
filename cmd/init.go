package cmd

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/markbates/pkger"
	"github.com/spf13/cobra"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initailize a new flask project",
	Run: func(cmd *cobra.Command, args []string) {
		var path string
		if len(args) > 0 {
			path, _ = filepath.Abs(args[0])
		} else {
			path, _ = os.Getwd()
		}

		info, err := os.Stat(path)
		if os.IsNotExist(err) {
			os.MkdirAll(path, 0777)
		}
		if err == nil {
			if !info.IsDir() {
				os.MkdirAll(path, 0777)
			}
		}

		err = pkger.Walk("/gen/src", func(p string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			f, err := pkger.Open(p)
			if err != nil {
				return err
			}
			relative := p[strings.Index(p, ":/gen")+6:]
			fullPath := filepath.Join(path, relative)

			fmt.Println(p)

			if info.IsDir() {
				err = os.MkdirAll(fullPath, 0777)
				if err != nil {
					return err
				}
				return nil
			}

			file, err := os.OpenFile(fullPath, os.O_RDWR|os.O_CREATE, 0666)
			if err != nil {
				return err
			}
			io.Copy(file, f)
			return nil
		})
		if err != nil {
			fmt.Println(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
