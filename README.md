# Flask-Gen
A simple flask project generator. It creates a flask project that is based off
of Miguel Grinberg's [flask mega tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) with several modifications. For an in depth look
at the structure of the flask project look at the [flask mega tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) by
[Miguel Grinberg](https://blog.miguelgrinberg.com/)


## Installation
If you have go installed you can use `go get github.com/Atrociously/flask-gen`,
otherwise install [go](https://golang.org/doc/install) then do that. May add
support for other installation methods in the future, but the current focus is
making flask-gen better and more robust.

## Usage
`flask-gen init` creates the flask project in the current directory.

`flask-gen init <path>` creates the flask project in the specified path.

## Current State
Currently flask-gen is in an extremely early stage and is purely experimental.
