module github.com/Atrociously/flask-gen

go 1.15

require (
	github.com/markbates/pkger v0.17.0
	github.com/spf13/cobra v1.0.0
)
