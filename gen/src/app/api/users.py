from flask import jsonify, request, abort
from app import db
from app.models import User
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request


@bp.route('/users/<int:id>', methods=['GET', 'PUT'])
@token_auth.login_required
def user(id):
    if request.method == 'GET':
        return jsonify(User.query.get_or_404(id).to_dict())

    if request.method == 'PUT':
        if token_auth.current_user().id != id:
            abort(403)
        user = User.query.get_or_404(id)
        data = request.get_json() or {}
        if 'username' in data and data['username'] != user.username and \
                User.query.filter_by(username=data['username']).first():
            return bad_request('please use a different username')
        if 'email' in data and data['email'] != user.email and \
                User.query.filter_by(email=data['email']).first():
            return bad_request('please use a different email address')
        user.from_dict(data, new_user=False)
        db.session.commit()
        return jsonify(user.to_dict())


@bp.route('/users', methods=['GET'])
@token_auth.login_required
def users():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
    return jsonify(data)
