package main

import (
	"github.com/Atrociously/flask-gen/cmd"
	"github.com/markbates/pkger"
)

func main() {
	pkger.Include("/gen")
	cmd.Execute()
}
